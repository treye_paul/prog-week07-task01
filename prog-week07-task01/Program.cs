﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task01
{
    class Program
    {
        static void method( )
        {
            Console.WriteLine("This is a method");
            Console.WriteLine("This second line is now printed to the screen");

        }
        static void Main(string[] args)
        {
            method();
        }
    }
}
